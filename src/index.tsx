import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";
import StubUserApi from "./tests/stubs/StubUserApi";
import StubSessionApi from "./tests/stubs/StubSessionApi";
import StubTaskApi from "./tests/stubs/StubTaskApi";
import { TaskStatus } from "./core/ITaskApi";

const sessionApi = new StubSessionApi();
const userApi = new StubUserApi();
const taskApi = new StubTaskApi();

const users = [
  { fullName: "Jonathan", email: "some@email.com" },
  { fullName: "Stacy", email: "some1@email.com" },
  { fullName: "Another Jonathan", email: "some2@email.com" },
];

const tasks = [
  {
    id: "AP-1",
    title: "Fix task display visuals",
    description: "The task display is way too ugly. Fix it.",
    assignees: ["Assignee 1", "Assignee 2"],
    author: "Author 1",
    due: new Date(),
    status: TaskStatus.DONE,
  },
  {
    id: "2",
    title: "T-2",
    description: "I is task",
    assignees: ["Assignee 1"],
    author: "Author 1",
    due: new Date(),
    status: TaskStatus.DONE,
  },
  {
    id: "3",
    title: "T-3",
    description: "I is task.",
    assignees: ["Assignee 1", "Assignee 2"],
    author: "Author 1",
    due: new Date(),
    status: TaskStatus.NOT_DONE,
  },
  {
    id: "4",
    title: "T-4",
    description: "I is task",
    assignees: ["Assignee 1"],
    author: "Author 1",
    due: new Date(),
    status: TaskStatus.IN_PROGRESS,
  },
  {
    id: "5",
    title: "T-5",
    description: "I is task",
    assignees: ["Assignee 1", "Assignee 2"],
    author: "Author 1",
    due: new Date(),
    status: TaskStatus.NOT_DONE,
  },
];

userApi.users = users;
sessionApi.user = { email: "email@domain.com", fullName: "James" };
taskApi.assigneeTasks = tasks.slice(0, 3);
taskApi.authorTasks = tasks.slice(3);

tasks.forEach((task) => taskApi.taskDetails.set(task.id, task));

ReactDOM.render(
  <React.StrictMode>
    <App sessionApi={sessionApi} userApi={userApi} taskApi={taskApi} />
  </React.StrictMode>,
  document.getElementById("root")
);
