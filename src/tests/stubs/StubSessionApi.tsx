import { ISessionApi, LoginDetails, UserDetails } from "../../core/ISessionApi";

class StubSessionApi implements ISessionApi {
  user?: UserDetails | null;
  getLoggedInPromise?: Promise<UserDetails>;
  hasLoggedInPromise?: Promise<boolean>;

  approveLogin: boolean = false;
  login?: LoginDetails;
  loginPromise?: Promise<boolean>;

  approveLogout: boolean = true;
  logoutPromise?: Promise<boolean>;

  loginUser = (login: LoginDetails) => {
    this.login = login;
    return (this.loginPromise = Promise.resolve(this.approveLogin));
  };

  getLoggedInUser = () =>
    (this.getLoggedInPromise = Promise.resolve(this.user!!));

  hasLoggedInUser = () =>
    (this.hasLoggedInPromise = Promise.resolve(this.user != null));

  logoutUser(): Promise<boolean> {
    this.user = null;
    return (this.logoutPromise = Promise.resolve(this.approveLogout));
  }
}

export default StubSessionApi;
