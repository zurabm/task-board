import {
  IUserApi,
  RegistrationDetails,
  UserMetadata,
} from "../../core/IUserApi";

class StubUserApi implements IUserApi {
  approveRegistration: boolean = true;
  registration?: RegistrationDetails;
  registrationPromise?: Promise<boolean>;

  users: UserMetadata[] = [];
  usersPromise?: Promise<UserMetadata[]>;

  registerUser = (registration: RegistrationDetails) => {
    this.registration = registration;
    this.registrationPromise = Promise.resolve(this.approveRegistration);
    return this.registrationPromise;
  };

  getUsers(): Promise<UserMetadata[]> {
    return (this.usersPromise = Promise.resolve(this.users));
  }
}

export default StubUserApi;
