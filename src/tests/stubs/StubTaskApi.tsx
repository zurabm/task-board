import {
  CreateTaskDetails,
  ITaskApi,
  Task,
  TaskFilter,
  TaskMetadata,
} from "../../core/ITaskApi";

class StubTaskApi implements ITaskApi {
  assigneeTasks: TaskMetadata[] = [];
  authorTasks: TaskMetadata[] = [];
  dueSoonTasks: TaskMetadata[] = [];
  tasksPromise?: Promise<TaskMetadata[]>;

  taskDetails: Map<string, Task> = new Map();
  taskDetailsPromise?: Promise<Task>;

  createTaskSuccessfully: boolean = true;
  createTaskDetails?: CreateTaskDetails;
  createTaskPromise?: Promise<boolean>;

  getTasks = async (filter?: TaskFilter) => {
    await this.tasksPromise?.finally();

    if (filter?.assignees) {
      this.tasksPromise = Promise.resolve(this.assigneeTasks);
    } else if (filter?.author) {
      this.tasksPromise = Promise.resolve(this.authorTasks);
    } else {
      this.tasksPromise = Promise.resolve(this.dueSoonTasks);
    }
    return this.tasksPromise;
  };

  getTask = (taskId: string) => {
    return (this.taskDetailsPromise = Promise.resolve(
      this.taskDetails.get(taskId)!!
    ));
  };

  createTask(task: CreateTaskDetails): Promise<boolean> {
    this.createTaskDetails = task;
    return (this.createTaskPromise = Promise.resolve(
      this.createTaskSuccessfully
    ));
  }
}

export default StubTaskApi;
