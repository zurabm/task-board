import React from "react";
import { render, screen, waitFor } from "@testing-library/react";
import App from "../components/App";
import userEvent from "@testing-library/user-event";
import StubUserApi from "./stubs/StubUserApi";
import StubSessionApi from "./stubs/StubSessionApi";
import StubTaskApi from "./stubs/StubTaskApi";
import { TaskStatus } from "../core/ITaskApi";
import { act } from "react-dom/test-utils";

let sessionApi: StubSessionApi;
let userApi: StubUserApi;
let taskApi: StubTaskApi;

beforeEach(() => {
  sessionApi = new StubSessionApi();
  userApi = new StubUserApi();
  taskApi = new StubTaskApi();
});

/**
 * Creates the app with stubbed dependencies.
 */
const app = () => (
  <App sessionApi={sessionApi} userApi={userApi} taskApi={taskApi} />
);

/**
 * Retrieves a button from the screen with the specified text.
 */
const getButton = (name: string | RegExp) => {
  return screen.getByRole("button", { name: name });
};

/**
 * Queries a button from the screen with the specified text.
 */
const queryButton = (name: string | RegExp) => {
  return screen.queryByRole("button", { name: name });
};

/**
 * Finds a button on the screen with the specified text.
 */
const findButton = (name: string | RegExp) => {
  return screen.findByRole("button", { name: name });
};

/**
 * Returns the current time instant.
 */
const now = () => new Date();

/**
 * Awaits the result of processing a user registration.
 */
const registrationResource = async () => {
  await act(async () => {
    await userApi.registrationPromise?.finally();
  });
};

/**
 * Awaits the result of processing a user login.
 */
const loginResource = async () => {
  await act(async () => {
    await sessionApi.loginPromise?.finally();
  });
};

/**
 * Awaits the result of processing a user logout.
 */
const logoutResource = async () => {
  await act(async () => {
    await sessionApi.logoutPromise?.finally();
  });
};

/**
 * Awaits the result of retrieving session information.
 */
const sessionResources = async () => {
  await act(async () => {
    await sessionApi.hasLoggedInPromise?.finally();
    await sessionApi.getLoggedInPromise?.finally();
  });
};

/**
 * Awaits the result of retrieving tasks.
 */
const tasksResources = async () => {
  await act(async () => {
    await taskApi.tasksPromise?.finally();
    await taskApi.taskDetailsPromise?.finally();
    await taskApi.createTaskPromise?.finally();
  });
};

/**
 * Awaits the result of retrieving information on users.
 */
const usersResource = async () => {
  await act(async () => {
    await userApi.usersPromise?.finally();
  });
};

describe("Application", () => {
  test("renders welcome text", () => {
    render(app());

    const welcomeText = screen.getByText(/.*task.*board/i);

    expect(welcomeText).toBeInTheDocument();
  });
});

describe("Registration", () => {
  /**
   * Validates that all components of the registration form render on the screen.
   */
  const expectRegistrationForm = () => {
    expect(screen.getByLabelText(/full name/i)).toBeInTheDocument();
    expect(screen.getByLabelText(/password/i)).toBeInTheDocument();
    expect(screen.getByLabelText(/email/i)).toBeInTheDocument();
    expect(getButton(/register/i)).toBeInTheDocument();
    expect(getButton(/close/i)).toBeInTheDocument();
  };

  /**
   * Validates that no components of the registration form render on the screen.
   */
  const expectNoRegistrationForm = async () => {
    await waitFor(() =>
      expect(screen.queryByLabelText(/full name/i)).not.toBeInTheDocument()
    );
    await waitFor(() =>
      expect(screen.queryByLabelText(/password/i)).not.toBeInTheDocument()
    );
    await waitFor(() =>
      expect(screen.queryByLabelText(/email/i)).not.toBeInTheDocument()
    );
    await waitFor(() =>
      expect(queryButton(/register/i)).not.toBeInTheDocument()
    );
    await waitFor(() => expect(queryButton(/close/i)).not.toBeInTheDocument());
  };

  test("renders registration button", () => {
    render(app());

    const registerButton = getButton(/sign up/i);

    expect(registerButton).toBeInTheDocument();
  });

  test("renders registration form", async () => {
    render(app());

    await expectNoRegistrationForm();

    userEvent.click(getButton(/sign up/i));

    await expectRegistrationForm();
  });

  test("does not render registration form after clicking the close button", async () => {
    render(app());

    userEvent.click(getButton(/sign up/i));
    userEvent.click(getButton(/close/i));

    await expectNoRegistrationForm();
  });

  test.each`
    expectSuccess
    ${true}
    ${false}
  `(
    "submits registration details after clicking the register button",
    async ({ expectSuccess }) => {
      userApi.approveRegistration = expectSuccess;

      render(app());

      userEvent.click(getButton(/sign up/i));

      userEvent.click(screen.getByLabelText(/full name/i));
      userEvent.keyboard("Test User");
      userEvent.click(screen.getByLabelText(/password/i));
      userEvent.keyboard("Password");
      userEvent.click(screen.getByLabelText(/email/i));
      userEvent.keyboard("email@domain.com");

      userEvent.click(getButton(/register/i));

      await registrationResource();

      const result = screen.getByText(expectSuccess ? /success/i : /failure/i);
      const closeButton = getButton(/close/i);

      expect(result).toBeInTheDocument();
      expect(closeButton).toBeInTheDocument();
      expect(userApi.registration).toEqual({
        fullName: "Test User",
        password: "Password",
        email: "email@domain.com",
      });
    }
  );

  test.each`
    expectSuccess
    ${true}
    ${false}
  `(
    "renders new registration form after submission",
    async ({ expectSuccess }) => {
      userApi.approveRegistration = expectSuccess;

      render(app());

      userEvent.click(getButton(/sign up/i));
      userEvent.click(getButton(/register/i));
      userEvent.click(getButton(/close/i));

      await expectNoRegistrationForm();

      userEvent.click(getButton(/sign up/i));

      expectRegistrationForm();
    }
  );
});

describe("Login", () => {
  /**
   * Validates that all components of the login form render on the screen.
   */
  const expectLoginForm = () => {
    expect(screen.getByLabelText(/email/i)).toBeInTheDocument();
    expect(screen.getByLabelText(/password/i)).toBeInTheDocument();
    expect(getButton(/log in/i)).toBeInTheDocument();
    expect(getButton(/close/i)).toBeInTheDocument();
  };

  /**
   * Validates that no components of the login form render on the screen.
   */
  const expectNoLoginForm = async () => {
    await waitFor(() =>
      expect(screen.queryByLabelText(/email/i)).not.toBeInTheDocument()
    );
    await waitFor(() =>
      expect(screen.queryByLabelText(/password/i)).not.toBeInTheDocument()
    );
    await waitFor(() => expect(queryButton(/log in/i)).not.toBeInTheDocument());
    await waitFor(() => expect(queryButton(/close/i)).not.toBeInTheDocument());
  };

  test("renders sign in button", () => {
    render(app());

    const loginButton = getButton(/sign in/i);

    expect(loginButton).toBeInTheDocument();
  });

  test("renders sign in form", async () => {
    render(app());

    await expectNoLoginForm();

    userEvent.click(getButton(/sign in/i));

    expectLoginForm();
  });

  test("does not render login form after clicking the close button", async () => {
    render(app());

    userEvent.click(getButton(/sign in/i));
    userEvent.click(getButton(/close/i));

    await expectNoLoginForm();
  });

  test("submits login details after clicking the submit button", async () => {
    render(app());

    userEvent.click(getButton(/sign in/i));
    userEvent.click(screen.getByLabelText(/email/i));
    userEvent.keyboard("email@domain.com");
    userEvent.click(screen.getByLabelText(/password/i));
    userEvent.keyboard("Password");

    userEvent.click(getButton(/log in/i));

    await loginResource();

    expect(sessionApi.login).toEqual({
      email: "email@domain.com",
      password: "Password",
    });
  });

  test("does not remove login form on failed login attempt", async () => {
    sessionApi.approveLogin = false;

    render(app());

    userEvent.click(getButton(/sign in/i));
    userEvent.click(screen.getByLabelText(/email/i));
    userEvent.keyboard("email@domain.com");
    userEvent.click(screen.getByLabelText(/password/i));
    userEvent.keyboard("Password");

    userEvent.click(getButton(/log in/i));

    await loginResource();

    const result = screen.getByText(/failure/i);

    expect(result).toBeInTheDocument();
    expectLoginForm();
  });

  test("removes login and registration elements on successful login", async () => {
    sessionApi.approveLogin = true;

    render(app());

    sessionApi.user = { email: "email@domain.com", fullName: "Bob" };

    userEvent.click(getButton(/sign in/i));
    userEvent.click(screen.getByLabelText(/email/i));
    userEvent.keyboard("email@domain.com");
    userEvent.click(screen.getByLabelText(/password/i));
    userEvent.keyboard("Password");

    userEvent.click(getButton(/log in/i));

    await loginResource();
    await tasksResources();

    const registerButton = queryButton(/sign up/i);
    const loginButton = queryButton(/sign in/i);

    await expectNoLoginForm();
    expect(registerButton).not.toBeInTheDocument();
    expect(loginButton).not.toBeInTheDocument();
  });
});

describe("User Page", function () {
  const TASK = {
    id: "Task #1",
    title: "Task Title",
    description: "A task",
    due: now(),
    assignees: ["Assignee 1", "Assignee 2"],
    author: "This Author",
    status: TaskStatus.DONE,
  };

  /**
   * Validates that all columns of the task table are rendered on screen.
   */
  const expectTasksTable = () => {
    expect(screen.getAllByText(/.*id.*/i)).not.toHaveLength(0);
    expect(screen.getAllByText(/.*title.*/i)).not.toHaveLength(0);
    expect(screen.getAllByText(/.*due.*/i)).not.toHaveLength(0);
    expect(screen.getAllByText(/.*status.*/i)).not.toHaveLength(0);
  };

  beforeEach(() => {
    sessionApi.user = { email: "email@domain.com", fullName: "Bob" };
    taskApi.assigneeTasks = [];
    taskApi.authorTasks = [];
  });

  test("renders welcome text for user", async () => {
    render(app());

    await sessionResources();

    const welcomeText = screen.getAllByText(/bob/i);
    const menuButton = getButton(/tasks/i);

    expect(welcomeText).not.toHaveLength(0);
    expect(menuButton).toBeInTheDocument();
  });

  test("renders task navigation buttons for user", async () => {
    render(app());

    await sessionResources();

    userEvent.click(getButton(/tasks/i));

    const homeButton = await findButton(/.*home.*/i);
    const assignedTasksButton = await findButton(/.*assigned.*/i);
    const authoredTasksButton = await findButton(/.*created.*/i);
    const dueSoonTasksButton = await findButton(/.*due.*/i);

    expect(homeButton).toBeInTheDocument();
    expect(assignedTasksButton).toBeInTheDocument();
    expect(authoredTasksButton).toBeInTheDocument();
    expect(dueSoonTasksButton).toBeInTheDocument();
  });

  test("navigates back to welcome text after changing tabs", async () => {
    render(app());

    await sessionResources();

    userEvent.click(getButton(/tasks/i));
    userEvent.click(await findButton(/.*assigned.*/i));
    userEvent.click(getButton(/.*close.*/i));

    userEvent.click(await findButton(/tasks/i));
    userEvent.click(await findButton(/.*home.*/i));
    userEvent.click(getButton(/.*close.*/i));

    const welcomeText = screen.getAllByText(/bob/i);

    expect(welcomeText).not.toHaveLength(0);
  });

  test("renders assigned tasks", async () => {
    taskApi.assigneeTasks = [
      { id: "1", title: "T-1", due: now(), status: TaskStatus.DONE },
      { id: "2", title: "T-2", due: now(), status: TaskStatus.DONE },
      { id: "3", title: "T-3", due: now(), status: TaskStatus.NOT_DONE },
    ];

    render(app());

    await sessionResources();

    userEvent.click(getButton(/tasks/i));
    userEvent.click(await findButton(/.*assigned.*/i));

    await tasksResources();

    const assignedTasksText = screen.getByText(/.*tasks.*assigned.*/i);

    const task1 = await screen.findByText(/.*T-1.*/i);
    const task2 = await screen.findByText(/.*T-2.*/i);
    const task3 = await screen.findByText(/.*T-3.*/i);

    expect(assignedTasksText).toBeInTheDocument();
    expectTasksTable();

    expect(task1).toBeInTheDocument();
    expect(task2).toBeInTheDocument();
    expect(task3).toBeInTheDocument();
  });

  test("renders authored tasks", async () => {
    taskApi.authorTasks = [
      { id: "4", title: "T-4", due: now(), status: TaskStatus.IN_PROGRESS },
      { id: "5", title: "T-5", due: now(), status: TaskStatus.NOT_DONE },
    ];

    render(app());

    await sessionResources();

    userEvent.click(getButton(/tasks/i));
    userEvent.click(await findButton(/.*created.*/i));

    await tasksResources();

    const createdTasksText = screen.getByText(/.*tasks.*created.*/i);
    const task1 = await screen.findByText(/.*T-4.*/i);
    const task2 = await screen.findByText(/.*T-5.*/i);

    expect(createdTasksText).toBeInTheDocument();
    expectTasksTable();

    expect(task1).toBeInTheDocument();
    expect(task2).toBeInTheDocument();
  });

  test("renders due soon tasks", async () => {
    taskApi.dueSoonTasks = [
      { id: "4", title: "T-4", due: now(), status: TaskStatus.IN_PROGRESS },
      { id: "5", title: "T-5", due: now(), status: TaskStatus.NOT_DONE },
    ];

    render(app());

    await sessionResources();

    userEvent.click(getButton(/tasks/i));
    userEvent.click(await findButton(/.*due.*/i));

    await tasksResources();

    const dueTasksText = screen.getByText(/.*tasks.*due.*soon.*/i);
    const task1 = await screen.findByText(/.*T-4.*/i);
    const task2 = await screen.findByText(/.*T-5.*/i);

    expect(dueTasksText).toBeInTheDocument();
    expectTasksTable();

    expect(task1).toBeInTheDocument();
    expect(task2).toBeInTheDocument();
  });

  test("renders tab for creating tasks", async () => {
    render(app());

    await sessionResources();

    userEvent.click(getButton(/tasks/i));
    userEvent.click(await findButton(/.*create.*task.*/i));
    userEvent.click(await findButton(/.*close.*/i));

    const createTaskText = screen.getAllByText(/.*create.*task.*/i);
    const titleText = screen.getByLabelText(/.*title.*/i);
    const descriptionText = screen.getByLabelText(/.*description.*/i);
    const dueDateText = screen.getByLabelText(/.*due.*date.*/i);
    const assigneesText = screen.getByLabelText(/.*assignees.*/i);
    const statusText = screen.getByLabelText(/.*status.*/i);
    const createTaskButton = getButton(/.*create.*/i);

    expect(createTaskText).not.toHaveLength(0);
    expect(titleText).toBeInTheDocument();
    expect(descriptionText).toBeInTheDocument();
    expect(dueDateText).toBeInTheDocument();
    expect(assigneesText).toBeInTheDocument();
    expect(statusText).toBeInTheDocument();
    expect(createTaskButton).toBeInTheDocument();
  });

  test.each`
    expectSuccess
    ${true}
    ${false}
  `("submits task details on creation", async ({ expectSuccess }) => {
    taskApi.createTaskSuccessfully = expectSuccess;
    const EXPECTED_TASK = {
      title: "A task",
      description: "A task description",
      due: now(),
      assignees: ["Assignee 1", "Assignee 2"],
      status: TaskStatus.DONE,
    };

    render(app());

    await sessionResources();

    userEvent.click(getButton(/tasks/i));
    userEvent.click(await findButton(/.*create.*task.*/i));
    userEvent.click(await findButton(/.*close.*/i));

    userEvent.click(screen.getByLabelText(/.*title.*/i));
    userEvent.keyboard(EXPECTED_TASK.title);
    userEvent.click(screen.getByLabelText(/.*description.*/i));
    userEvent.keyboard(EXPECTED_TASK.description);
    // TODO: Check other fields as well.

    userEvent.click(getButton(/.*create.*/i));

    await tasksResources();

    const successText = screen.getByText(
      expectSuccess ? /.*success.*/i : /.*fail.*/i
    );

    expect(taskApi.createTaskDetails?.title).toEqual(EXPECTED_TASK.title);
    expect(taskApi.createTaskDetails?.description).toEqual(
      EXPECTED_TASK.description
    );
    expect(successText).toBeInTheDocument();
  });

  test("renders potential assignees when creating a task", async () => {
    userApi.users = [
      { fullName: "User 1", email: "user1@email.com" },
      { fullName: "User 2", email: "user2@email.com" },
      { fullName: "User 3", email: "user3@email.com" },
    ];

    render(app());

    await sessionResources();

    userEvent.click(getButton(/tasks/i));
    userEvent.click(await findButton(/.*create.*task.*/i));
    userEvent.click(await findButton(/.*close.*/i));

    await usersResource();

    // TODO: check that the list of users is actually rendered.
    expect(userApi.usersPromise).not.toBeNull();
  });

  test("re-renders tables when switching between tabs", async () => {
    render(app());

    await sessionResources();

    userEvent.click(getButton(/tasks/i));
    userEvent.click(await findButton(/.*assigned.*/i));

    userEvent.click(await findButton(/.*created.*/i));
    await tasksResources();

    expect(screen.getByText(/.*tasks.*created.*/i)).toBeInTheDocument();

    userEvent.click(await findButton(/.*due.*/i));
    await tasksResources();

    expect(screen.getByText(/.*tasks.*due.*soon.*/i)).toBeInTheDocument();
  });

  test("renders task details on click", async () => {
    const TEST_TASK = { ...TASK, status: TaskStatus.IN_PROGRESS };
    taskApi.assigneeTasks = [TEST_TASK];
    taskApi.taskDetails.set(TEST_TASK.id, TEST_TASK);

    render(app());

    await sessionResources();

    userEvent.click(getButton(/tasks/i));
    userEvent.click(await findButton(/.*assigned.*/i));
    userEvent.click(await findButton(/.*close.*/i));

    await tasksResources();

    userEvent.click(await screen.findByText(TEST_TASK.title));

    await tasksResources();

    const idText = screen.getAllByText(TEST_TASK.id);
    const titleText = screen.getAllByText(TEST_TASK.title);
    const descriptionText = screen.getByText(TEST_TASK.description);
    const assigneeText1 = screen.getByText(TEST_TASK.assignees[0], {
      exact: false,
    });
    const assigneeText2 = screen.getByText(TEST_TASK.assignees[1], {
      exact: false,
    });
    const authorText = screen.getByText(TEST_TASK.author);
    const statusText = screen.getAllByText(/in progress/i);

    expect(idText).not.toHaveLength(0);
    expect(titleText).not.toHaveLength(0);
    expect(descriptionText).toBeInTheDocument();
    expect(assigneeText1).toBeInTheDocument();
    expect(assigneeText2).toBeInTheDocument();
    expect(authorText).toBeInTheDocument();
    expect(statusText).not.toHaveLength(0);
  });

  test("closes task details on click", async () => {
    taskApi.assigneeTasks = [TASK];
    taskApi.taskDetails.set(TASK.id, TASK);

    render(app());

    await sessionResources();

    userEvent.click(getButton(/tasks/i));
    userEvent.click(await findButton(/.*assigned.*/i));
    userEvent.click(getButton(/close/i));

    await tasksResources();

    userEvent.click(await screen.findByText(TASK.title));

    await tasksResources();

    userEvent.click(await screen.findByText(/close/i));

    const descriptionText = screen.queryByText(TASK.description);
    const assigneeText1 = screen.queryByText(TASK.assignees[0], {
      exact: false,
    });
    const assigneeText2 = screen.queryByText(TASK.assignees[1], {
      exact: false,
    });
    const authorText = screen.queryByText(TASK.author);

    expect(descriptionText).not.toBeInTheDocument();
    expect(assigneeText1).not.toBeInTheDocument();
    expect(assigneeText2).not.toBeInTheDocument();
    expect(authorText).not.toBeInTheDocument();
  });
});

describe("Logout", () => {
  beforeEach(
    () => (sessionApi.user = { email: "email@domain.com", fullName: "Bob" })
  );

  test("renders log out button", async () => {
    render(app());

    await sessionResources();

    userEvent.click(getButton(/tasks/i));
    const logOutButton = await findButton(/.*log.*out.*/i);

    expect(logOutButton).toBeInTheDocument();
  });

  test("renders confirmation buttons when logging out", async () => {
    render(app());

    await sessionResources();

    userEvent.click(getButton(/tasks/i));
    userEvent.click(await findButton(/.*log.*out.*/i));

    const confirmationText = await screen.findByText(/.*confirm.*log.*out/i);
    const yesButton = await findButton(/yes/i);
    const noButton = await findButton(/no/i);

    expect(confirmationText).toBeInTheDocument();
    expect(yesButton).toBeInTheDocument();
    expect(noButton).toBeInTheDocument();
  });

  test("closes log out dialog after clicking no", async () => {
    render(app());

    await sessionResources();

    userEvent.click(getButton(/tasks/i));
    userEvent.click(await findButton(/.*log.*out.*/i));
    userEvent.click(await findButton(/.*no.*/i));

    await logoutResource();

    expect(sessionApi.user).not.toBeNull();
    await waitFor(() =>
      expect(screen.queryByText(/.*confirm.*log.*out/i)).not.toBeInTheDocument()
    );
  });

  test("logs out from user's account after clicking log out", async () => {
    render(app());

    await sessionResources();

    userEvent.click(getButton(/tasks/i));
    userEvent.click(await findButton(/.*log.*out.*/i));
    userEvent.click(await findButton(/.*yes.*/i));

    await logoutResource();

    const tasksButton = queryButton(/tasks/i);

    expect(sessionApi.user).toBeNull();
    expect(tasksButton).not.toBeInTheDocument();
  });
});
