enum TaskStatus {
  NOT_DONE = "NOT_DONE",
  IN_PROGRESS = "IN_PROGRESS",
  DONE = "DONE",
}

interface TaskMetadata {
  id: string;
  title: string;
  due: Date;
  status: TaskStatus;
}

interface Task {
  id: string;
  title: string;
  description: string;
  due: Date;
  assignees: string[];
  author: string;
  status: TaskStatus;
}

interface TaskFilter {
  assignees?: string[];
  author?: string;
  overdue?: boolean;
  timeLeft?: Duration;
}

interface CreateTaskDetails {
  title: string;
  description: string;
  due?: Date;
  assignees: string[];
  status: TaskStatus;
}

interface ITaskApi {
  /**
   * Retrieves all tasks.
   */
  getTasks(filter?: TaskFilter): Promise<TaskMetadata[]>;

  /**
   * Retrieves full information about the task with the specified ID.
   */
  getTask(taskId: string): Promise<Task>;

  /**
   * Creates a task with the specified details.
   */
  createTask(task: CreateTaskDetails): Promise<boolean>;
}

export type { TaskMetadata, Task, CreateTaskDetails, TaskFilter, ITaskApi };
export { TaskStatus };
