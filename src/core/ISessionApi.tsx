interface LoginDetails {
  readonly email: string;
  readonly password: string;
}

interface UserDetails {
  readonly email: string;
  readonly fullName: string;
}

interface ISessionApi {
  /**
   * Authenticates a user with the specified {@link LoginDetails} and returns the result.
   */
  loginUser(login: LoginDetails): Promise<boolean>;

  /**
   * Returns true if a session for a logged-in user exists.
   */
  hasLoggedInUser(): Promise<boolean>;

  /**
   * Retrieves the currently logged-in user details.
   */
  getLoggedInUser(): Promise<UserDetails>;

  /**
   * Logs out for the current user.
   */
  logoutUser(): Promise<boolean>;
}

export type { LoginDetails, UserDetails, ISessionApi };
