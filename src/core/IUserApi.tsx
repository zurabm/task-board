interface RegistrationDetails {
  readonly fullName: string;
  readonly email: string;
  readonly password: string;
}

interface UserMetadata {
  readonly fullName: string;
  readonly email: string;
}

interface IUserApi {
  /**
   * Attempts to register a user with the specified {@link RegistrationDetails} and returns the result.
   */
  registerUser(registration: RegistrationDetails): Promise<boolean>;

  /**
   * Retrieves metadata about all users in the system.
   */
  getUsers(): Promise<UserMetadata[]>;
}

export type { RegistrationDetails, UserMetadata, IUserApi };
