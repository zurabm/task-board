import React from "react";
import { RegistrationDetails } from "../../core/IUserApi";
import Form from "./Form";

type OnCloseListener = () => void;
type OnSubmitListener = (registration: RegistrationDetails) => void;

interface RegistrationFormProperties {
  readonly onClose: OnCloseListener;
  readonly onSubmit: OnSubmitListener;
}

function RegistrationForm({
  onSubmit,
  onClose,
}: RegistrationFormProperties): JSX.Element {
  /**
   * Submits registration details to the registered {@link OnSubmitListener}.
   */
  const handleSubmit = (formValues: { [field: string]: string }) => {
    onSubmit({
      email: formValues.email,
      fullName: formValues.fullName,
      password: formValues.password,
    });
  };

  return (
    <Form
      fields={[
        // TODO: All form fields are known at compile time. Change it to be more typesafe.
        { name: "email", label: "Email", type: "normal" },
        { name: "fullName", label: "Full Name", type: "normal" },
        { name: "password", label: "Password", type: "password" },
      ]}
      submitLabel="Register"
      onClose={onClose}
      onSubmit={handleSubmit}
    />
  );
}

export default RegistrationForm;
