import React from "react";
import Form from "./Form";
import { LoginDetails } from "../../core/ISessionApi";

type OnCloseListener = () => void;
type OnSubmitListener = (login: LoginDetails) => void;

interface LoginFormProperties {
  readonly onClose: OnCloseListener;
  readonly onSubmit: OnSubmitListener;
}

function LoginForm({ onSubmit, onClose }: LoginFormProperties): JSX.Element {
  /**
   * Submits login details to the registered {@link OnSubmitListener}.
   */
  const handleSubmit = (formValues: { [field: string]: string }) => {
    onSubmit({
      email: formValues.email,
      password: formValues.password,
    });
  };

  return (
    <Form
      fields={[
        // TODO: All form fields are known at compile time. Change it to be more typesafe.
        { name: "email", label: "Email", type: "normal" },
        { name: "password", label: "Password", type: "password" },
      ]}
      submitLabel="Log in"
      onClose={onClose}
      onSubmit={handleSubmit}
    />
  );
}

export default LoginForm;
