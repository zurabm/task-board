import { Button, Divider, Stack, TextField } from "@mui/material";
import AlignedContainer from "../AlignedContainer";
import React, { ChangeEvent, useState } from "react";

type OnCloseListener = () => void;
type OnSubmitListener = (formValues: { [field: string]: string }) => void;

interface FormField {
  readonly name: string;
  readonly label: string;
  readonly type: "normal" | "password";
}

interface FormProperties {
  readonly fields: FormField[];
  readonly submitLabel: string;
  readonly onClose: OnCloseListener;
  readonly onSubmit: OnSubmitListener;
}

function Form({
  fields,
  submitLabel,
  onClose,
  onSubmit,
}: FormProperties): JSX.Element {
  const [inputs, setInputs] = useState(
    Object.assign({}, ...fields.map((field) => ({ [field.name]: "" })))
  );

  /**
   * Creates a function that will update a string field with the specified name.
   */
  const updateField = (fieldName: string) => {
    return (event: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
      setInputs((inputs: { [field: string]: string }) => ({
        ...inputs,
        [fieldName]: event.target.value,
      }));
    };
  };

  return (
    <Stack gap={2}>
      <Divider />
      {fields.map((field, index) => (
        <TextField
          key={index}
          onChange={updateField(field.name)}
          label={field.label}
          type={field.type}
        />
      ))}
      <Divider />
      <AlignedContainer>
        <Button onClick={onClose}>Close</Button>
        <Button onClick={() => onSubmit(inputs)}>{submitLabel}</Button>
      </AlignedContainer>
    </Stack>
  );
}

export default Form;
