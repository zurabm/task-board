import { CreateTaskDetails, TaskStatus } from "../../core/ITaskApi";
import { Box, Button, Grid, TextField } from "@mui/material";
import { DateTimePicker, LocalizationProvider } from "@mui/lab";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import MultiSelectChip from "../inputs/MultiSelectChip";
import SimpleSelect from "../inputs/SimpleSelect";
import StatusDisplay from "../StatusDisplay";
import { useState } from "react";
import TextArea from "../inputs/TextArea";
import { UserMetadata } from "../../core/IUserApi";

type OnSubmitListener = (task: CreateTaskDetails) => void;

const statuses = [
  {
    value: TaskStatus.NOT_DONE,
    label: <StatusDisplay status={TaskStatus.NOT_DONE} asChip={false} />,
  },
  {
    value: TaskStatus.IN_PROGRESS,
    label: <StatusDisplay status={TaskStatus.IN_PROGRESS} asChip={false} />,
  },
  {
    value: TaskStatus.DONE,
    label: <StatusDisplay status={TaskStatus.DONE} asChip={false} />,
  },
];

interface PaddedBoxProperties {
  readonly children?: any;
  readonly light: boolean;
}

function PaddedBox({ children, light }: PaddedBoxProperties): JSX.Element {
  return <Box margin={light ? 1 : 2}>{children}</Box>;
}

PaddedBox.defaultProps = {
  light: false,
};

interface CreateTaskFormProperties {
  readonly users: UserMetadata[];
  readonly onSubmit: OnSubmitListener;
}

function CreateTaskForm({
  users,
  onSubmit,
}: CreateTaskFormProperties): JSX.Element {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [dueDate, setDueDate] = useState<Date | null>(null);
  const [assignees, setAssignees] = useState<string[]>([]);
  const [status, setStatus] = useState<TaskStatus>(TaskStatus.NOT_DONE);

  /**
   * Submits task creation details to the registered {@link OnSubmitListener}.
   */
  const handleSubmit = () => {
    onSubmit({
      title: title,
      description: description,
      due: dueDate ?? undefined,
      assignees: assignees,
      status: status,
    });
  };

  return (
    <Box>
      <Grid container direction="row">
        <PaddedBox>
          <TextField
            label="Title"
            onChange={(event) => setTitle(event.target.value)}
          />
        </PaddedBox>
        <PaddedBox>
          <LocalizationProvider dateAdapter={AdapterDateFns}>
            <DateTimePicker
              label="Due Date"
              value={dueDate}
              onChange={setDueDate}
              renderInput={(params) => <TextField {...params} />}
            />
          </LocalizationProvider>
        </PaddedBox>
        <PaddedBox light>
          <MultiSelectChip
            label="Assignees"
            possibleValues={users.map((user) => user.fullName)}
            onChange={setAssignees}
          />
        </PaddedBox>
        <PaddedBox light>
          <SimpleSelect label="Status" items={statuses} onChange={setStatus} />
        </PaddedBox>
      </Grid>

      <PaddedBox>
        <Grid container flexDirection="column">
          <TextArea label="Description" onChange={setDescription} />
        </Grid>
        <PaddedBox>
          <Button variant="contained" onClick={handleSubmit}>
            Create
          </Button>
        </PaddedBox>
      </PaddedBox>
    </Box>
  );
}

export default CreateTaskForm;
