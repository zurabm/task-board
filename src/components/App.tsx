import React, { useEffect, useState } from "react";
import { IUserApi } from "../core/IUserApi";
import { ISessionApi, UserDetails } from "../core/ISessionApi";
import HomePage from "./pages/HomePage";
import UserPage from "./pages/UserPage";
import { ITaskApi } from "../core/ITaskApi";

interface AppProperties {
  readonly sessionApi: ISessionApi;
  readonly userApi: IUserApi;
  readonly taskApi: ITaskApi;
}

function App({ sessionApi, userApi, taskApi }: AppProperties) {
  const [user, setUser] = useState<UserDetails | null>(null);
  const clearUser = () => setUser(null);

  useEffect(() => {
    sessionApi.hasLoggedInUser().then((hasLoggedInUser) => {
      if (user == null && hasLoggedInUser) {
        sessionApi.getLoggedInUser().then(setUser);
      }
    });
  });

  return (
    <>
      {user == null && (
        <HomePage sessionApi={sessionApi} userApi={userApi} onLogin={setUser} />
      )}
      {user != null && (
        <UserPage
          user={user}
          taskApi={taskApi}
          userApi={userApi}
          sessionApi={sessionApi}
          onLogout={clearUser}
        />
      )}
    </>
  );
}

export default App;
