import { Typography } from "@mui/material";

interface TextProperties {
  readonly type: "h3" | "h4" | "body1";
  readonly children?: any;
}

function StyledText({ type, children }: TextProperties): JSX.Element {
  return (
    <Typography variant={type} component="div" gutterBottom>
      {children}
    </Typography>
  );
}

export default StyledText;
