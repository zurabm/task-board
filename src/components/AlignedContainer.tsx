import { Grid, GridDirection } from "@mui/material";
import React from "react";

interface AlignedContainerProperties {
  readonly horizontalAlignment: "center" | "left" | "right";
  readonly verticalAlignment: "center" | "bottom";
  readonly containerDirection: "vertical" | "horizontal";
  readonly children?: any;
  readonly margin?: number;
}

function AlignedContainer({
  horizontalAlignment,
  verticalAlignment,
  containerDirection,
  children,
  margin,
}: AlignedContainerProperties): JSX.Element {
  let alignItems;
  let justifyContent;
  let direction: GridDirection;

  switch (horizontalAlignment) {
    case "center":
      justifyContent = "space-evenly";
      break;
    case "left":
      justifyContent = "flex-start";
      break;
    case "right":
      justifyContent = "flex-end";
      break;
  }

  switch (verticalAlignment) {
    case "center":
      alignItems = "center";
      break;
    case "bottom":
      alignItems = "flex-end";
      break;
  }

  switch (containerDirection) {
    case "horizontal":
      direction = "row";
      break;
    case "vertical":
      direction = "column";
      break;
  }

  return (
    <Grid
      container
      direction={direction}
      alignItems={alignItems}
      justifyContent={justifyContent}
      margin={margin}
    >
      {children}
    </Grid>
  );
}

AlignedContainer.defaultProps = {
  horizontalAlignment: "center",
  verticalAlignment: "center",
  containerDirection: "horizontal",
};

export default AlignedContainer;
