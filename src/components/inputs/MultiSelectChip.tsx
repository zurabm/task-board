import * as React from "react";
import { useState } from "react";
import Box from "@mui/material/Box";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import { Chip } from "@mui/material";

interface MultiSelectChipProperties {
  readonly label: string;
  readonly possibleValues: string[];
  readonly onChange: (selection: string[]) => void;
}

function MultiSelectChip({
  label,
  possibleValues,
  onChange,
}: MultiSelectChipProperties) {
  const [selection, setSelection] = useState<string[]>([]);

  const handleChange = (event: SelectChangeEvent<typeof selection>) => {
    const input = event.target.value;
    const parsedInput = typeof input === "string" ? input.split(",") : input;

    setSelection(parsedInput);
    onChange(parsedInput);
  };

  return (
    <>
      <FormControl sx={{ m: 1, minWidth: 150 }}>
        <InputLabel htmlFor={`input-${label}`}>{label}</InputLabel>
        <Select
          multiple
          autoWidth
          disabled={possibleValues.length === 0}
          value={selection}
          onChange={handleChange}
          input={<OutlinedInput id={`input-${label}`} label={label} />}
          renderValue={(selected) => (
            <Box sx={{ display: "flex", flexWrap: "wrap", gap: 0.5 }}>
              {selected.map((value) => (
                <Chip key={value} label={value} />
              ))}
            </Box>
          )}
        >
          {possibleValues.map((value) => (
            <MenuItem key={value} value={value}>
              {value}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </>
  );
}

export default MultiSelectChip;
