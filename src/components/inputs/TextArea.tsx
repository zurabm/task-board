import { TextField } from "@mui/material";
import { ChangeEvent } from "react";

interface TextAreaProperties {
  readonly label: string;
  readonly minRows: number;
  readonly onChange: (input: string) => void;
}

function TextArea({
  label,
  minRows,
  onChange,
}: TextAreaProperties): JSX.Element {
  const handleOnChange = (event: ChangeEvent<HTMLTextAreaElement>) => {
    onChange(event.target.value);
  };

  return (
    <TextField
      label={label}
      onChange={handleOnChange}
      multiline
      minRows={minRows}
    />
  );
}

TextArea.defaultProps = {
  minRows: 6,
};

export default TextArea;
