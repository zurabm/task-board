import * as React from "react";
import InputLabel from "@mui/material/InputLabel";
import FormControl from "@mui/material/FormControl";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import { OutlinedInput } from "@mui/material";

interface Item<ValueT> {
  readonly value: ValueT;
  readonly label: JSX.Element;
}

interface SimpleSelectProperties<ValueT> {
  readonly label: string;
  readonly items: Item<ValueT>[];
  readonly onChange: (selection: ValueT) => void;
}

function SimpleSelect<ValueT>({
  label,
  items,
  onChange,
}: SimpleSelectProperties<ValueT>) {
  const [selectionIndex, setSelectionIndex] = React.useState("0");

  const itemsByIndex = new Map(
    items.map((item, index) => [`${index}`, item.value])
  );

  const handleChange = ({ target: { value } }: SelectChangeEvent) => {
    const selectedItem = itemsByIndex.get(value)!!;
    setSelectionIndex(value);
    onChange(selectedItem);
  };

  return (
    <div>
      <FormControl sx={{ m: 1, minWidth: 80 }}>
        <InputLabel htmlFor={`input-${label}`}>{label}</InputLabel>
        <Select
          value={selectionIndex}
          onChange={handleChange}
          autoWidth
          input={<OutlinedInput id={`input-${label}`} label={label} />}
        >
          {items.map((item, index) => (
            <MenuItem value={index} key={index}>
              {item.label}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  );
}

export default SimpleSelect;
