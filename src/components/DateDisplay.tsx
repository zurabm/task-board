import { format } from "date-fns";

interface DateDisplayProperties {
  readonly date: Date;
}

function DateDisplay({ date }: DateDisplayProperties): JSX.Element {
  return <>{format(date, "MMMM do, h:mm:ss a")}</>;
}

export default DateDisplay;
