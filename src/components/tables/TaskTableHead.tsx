import { TableCell, TableHead, TableRow } from "@mui/material";
import React from "react";

interface TaskTableHeadCellProperties {
  readonly children?: any;
}

function TaskTableHeadCell(props: TaskTableHeadCellProperties): JSX.Element {
  return (
    <TableCell align="center">
      <strong>{props.children}</strong>
    </TableCell>
  );
}

function TaskTableHead(): JSX.Element {
  return (
    <TableHead>
      <TableRow>
        <TaskTableHeadCell>Task ID</TaskTableHeadCell>
        <TaskTableHeadCell>Title</TaskTableHeadCell>
        <TaskTableHeadCell>Due Date</TaskTableHeadCell>
        <TaskTableHeadCell>Status</TaskTableHeadCell>
      </TableRow>
    </TableHead>
  );
}

export default TaskTableHead;
