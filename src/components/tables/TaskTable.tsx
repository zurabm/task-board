import {
  Box,
  Divider,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
} from "@mui/material";
import React, { useState } from "react";
import { Task, TaskMetadata } from "../../core/ITaskApi";
import AlignedContainer from "../AlignedContainer";
import TaskTableHead from "./TaskTableHead";
import TaskDialog from "../dialogs/TaskDialog";
import DateDisplay from "../DateDisplay";
import StatusDisplay from "../StatusDisplay";

type TaskClickListener = (taskId: string) => void;

interface TaskTableCellProperties {
  readonly children?: any;
}

function TaskTableCell({ children }: TaskTableCellProperties): JSX.Element {
  return <TableCell align="center">{children}</TableCell>;
}

interface TaskTableRowProperties {
  readonly id: string;
  readonly onClick: TaskClickListener;
  readonly children?: any;
}

function TaskTableRow({
  id,
  onClick,
  children,
}: TaskTableRowProperties): JSX.Element {
  return (
    <TableRow hover={true} key={id} onClick={() => onClick(id)}>
      {children}
    </TableRow>
  );
}

interface ITaskDetailsApi {
  /**
   * Retrieves full information about the task with the specified ID.
   */
  getTask(taskId: string): Promise<Task>;
}

interface TaskTableProperties {
  readonly tasks: TaskMetadata[];
  readonly taskDetailsApi: ITaskDetailsApi;
  readonly children?: any;
}

function TaskTable({
  tasks,
  taskDetailsApi,
  children,
}: TaskTableProperties): JSX.Element {
  const [selectedTask, setSelectedTask] = useState<Task | null>(null);

  const handleTaskClick = (taskId: string) => {
    taskDetailsApi.getTask(taskId).then((task) => setSelectedTask(task));
  };

  const clearSelectedTask = () => setSelectedTask(null);

  return (
    <AlignedContainer horizontalAlignment={"center"} margin={1}>
      <TableContainer component={Paper}>
        <Box margin={2}>
          {children}
          <Divider />
        </Box>
        <Table>
          <TaskTableHead />
          <TableBody>
            {tasks.map((task, index) => (
              <TaskTableRow id={task.id} onClick={handleTaskClick} key={index}>
                <TaskTableCell>{task.id}</TaskTableCell>
                <TaskTableCell>{task.title}</TaskTableCell>
                <TaskTableCell>
                  <DateDisplay date={task.due} />
                </TaskTableCell>
                <TaskTableCell>
                  <StatusDisplay status={task.status} />
                </TaskTableCell>
              </TaskTableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <TaskDialog task={selectedTask} onClose={clearSelectedTask} />
    </AlignedContainer>
  );
}

export type { ITaskDetailsApi };
export { TaskTable };
