import { Box, Tab, Tabs } from "@mui/material";
import React, { useState } from "react";

interface TabManagerProperties<LabelT> {
  readonly tabs: LabelT[];
  readonly onSelect: (selectedLabel: LabelT) => void;
  readonly initialTab: LabelT;
}

function LabeledTabManager<LabelT>({
  tabs,
  onSelect,
  initialTab,
}: TabManagerProperties<LabelT>): JSX.Element {
  const [selectedTab, setSelectedTab] = useState(
    tabs.findIndex((tab) => tab === initialTab)
  );

  const tabsByIndex = new Map(tabs.map((tab, index) => [index, tab]));

  const handleChange = (index: number) => {
    setSelectedTab(index);
    onSelect(tabsByIndex.get(index)!!);
  };

  return (
    <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
      <Tabs
        value={selectedTab}
        onChange={(_, index) => handleChange(index)}
        orientation="vertical"
      >
        {tabs.map((tab, index) => (
          <Tab label={tab} tabIndex={index} role="button" key={index} />
        ))}
      </Tabs>
    </Box>
  );
}

export default LabeledTabManager;
