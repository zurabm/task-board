import React, { Component } from "react";
import { TaskMetadata } from "../../core/ITaskApi";
import { ITaskDetailsApi, TaskTable } from "../tables/TaskTable";
import StyledText from "../StyledText";
import { RefreshRounded } from "@mui/icons-material";
import { IconButton } from "@mui/material";

type TaskSupplier = () => Promise<TaskMetadata[]>;

interface TasksTabProperties {
  readonly title: string;
  readonly taskSupplier: TaskSupplier;
  readonly taskDetailsApi: ITaskDetailsApi;
}

interface TasksTabState {
  readonly tasks: TaskMetadata[];
}

class TasksTab extends Component<TasksTabProperties, TasksTabState> {
  constructor(props: TasksTabProperties) {
    super(props);
    this.title = props.title;
    this.taskSupplier = props.taskSupplier;
    this.taskDetailsApi = props.taskDetailsApi;
    this.isCancelled = false;
    this.state = { tasks: [] };
  }

  componentDidMount() {
    this.refreshTasks();
  }

  render() {
    return (
      <TaskTable tasks={this.state.tasks} taskDetailsApi={this.taskDetailsApi}>
        <StyledText type="h4">{this.title}</StyledText>
        <IconButton>
          <RefreshRounded />
        </IconButton>
      </TaskTable>
    );
  }

  componentWillUnmount() {
    this.isCancelled = true;
  }

  /**
   * Retrieves tasks from the specified task supplier.
   */
  private refreshTasks = () => {
    this.taskSupplier().then((tasks) => {
      if (!this.isCancelled) {
        this.setState({ tasks: tasks });
      }
    });
  };

  private readonly title: string;
  private readonly taskSupplier: TaskSupplier;
  private readonly taskDetailsApi: ITaskDetailsApi;
  private isCancelled: boolean;
}

export default TasksTab;
