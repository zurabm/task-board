import { UserDetails } from "../../core/ISessionApi";
import AlignedContainer from "../AlignedContainer";
import StyledText from "../StyledText";

interface WelcomeTabProperties {
  readonly user: UserDetails;
}

function WelcomeTab({ user }: WelcomeTabProperties): JSX.Element {
  return (
    <AlignedContainer>
      <StyledText type="h3">
        Welcome {user.fullName}! Hit the "Tasks" button to view your tasks.
      </StyledText>
    </AlignedContainer>
  );
}

export default WelcomeTab;
