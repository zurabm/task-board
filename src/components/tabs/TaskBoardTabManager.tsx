import LabeledTabManager from "./LabeledTabManager";
import React from "react";

enum TaskBoardTab {
  WELCOME = "Home",
  ASSIGNED = "Assigned to me",
  AUTHORED = "Created by me",
  DUE_SOON = "Due soon",
  CREATE = "Create a task",
}

interface TaskBoardTabManagerProperties {
  readonly selectedTab: TaskBoardTab;
  readonly onSelect: (selectedTab: TaskBoardTab) => void;
}

function TaskBoardTabManager({
  onSelect,
  selectedTab,
}: TaskBoardTabManagerProperties): JSX.Element {
  return (
    <LabeledTabManager
      tabs={[
        TaskBoardTab.WELCOME,
        TaskBoardTab.ASSIGNED,
        TaskBoardTab.AUTHORED,
        TaskBoardTab.DUE_SOON,
        TaskBoardTab.CREATE,
      ]}
      onSelect={onSelect}
      initialTab={selectedTab}
    />
  );
}

export { TaskBoardTab, TaskBoardTabManager };
