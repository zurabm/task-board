import { Alert, Box, Collapse, Divider, Paper } from "@mui/material";
import StyledText from "../StyledText";
import CreateTaskForm from "../forms/CreateTaskForm";
import { CreateTaskDetails } from "../../core/ITaskApi";
import { Component } from "react";
import { UserMetadata } from "../../core/IUserApi";

const RESULT_ALERT_DELAY_MS = 5000;
const SUCCESS_DESCRIPTION = "Success! A new task was created.";
const FAILURE_DESCRIPTION = "Failed to create a new task.";

interface ICreateTaskApi {
  /**
   * Creates a task with the specified details.
   */
  createTask(task: CreateTaskDetails): Promise<boolean>;
}

interface IGetUsersApi {
  /**
   * Retrieves metadata about all users in the system.
   */
  getUsers(): Promise<UserMetadata[]>;
}

interface Message {
  readonly text: string;
  readonly success: boolean;
}

/**
 * Creates a success {@link Message} with the specified text.
 */
const success = (message: string): Message => ({
  text: message,
  success: true,
});

/**
 * Creates a failure {@link Message} with the specified text.
 */
const failure = (message: string): Message => ({
  text: message,
  success: false,
});

const EMPTY_MESSAGE = success("");

interface CreateTaskTabProperties {
  readonly createTaskApi: ICreateTaskApi;
  readonly getUsersApi: IGetUsersApi;
}

interface CreateTaskTabState {
  readonly showMessage: boolean;
  readonly message: Message;
  readonly users: UserMetadata[];
}

class CreateTaskTab extends Component<
  CreateTaskTabProperties,
  CreateTaskTabState
> {
  constructor(props: CreateTaskTabProperties) {
    super(props);
    this.createTaskApi = props.createTaskApi;
    this.getUsersApi = props.getUsersApi;
    this.isCancelled = false;
    this.state = { showMessage: false, message: EMPTY_MESSAGE, users: [] };
  }

  componentDidMount() {
    this.getUsersApi.getUsers().then((users) => {
      if (!this.isCancelled) {
        this.setState({ users: users });
      }
    });
  }

  render() {
    return (
      <Box margin={2}>
        <Paper>
          <Box margin={2}>
            <StyledText type="h4">Create a new Task</StyledText>
          </Box>
          <Divider />
          <CreateTaskForm
            users={this.state.users}
            onSubmit={this.handleSubmit}
          />
          <Divider />
          <Collapse in={this.state.showMessage}>
            <Alert severity={this.state.message.success ? "success" : "error"}>
              {this.state.message.text}
            </Alert>
          </Collapse>
        </Paper>
      </Box>
    );
  }

  componentWillUnmount() {
    this.isCancelled = true;
  }

  private handleSubmit = (task: CreateTaskDetails) => {
    this.createTaskApi.createTask(task).then((taskCreated) => {
      if (!this.isCancelled) {
        this.setState({
          showMessage: true,
          message: taskCreated
            ? success(SUCCESS_DESCRIPTION)
            : failure(FAILURE_DESCRIPTION),
        });
      }
    });
    setTimeout(() => {
      if (!this.isCancelled) {
        this.setState({ showMessage: false });
      }
    }, RESULT_ALERT_DELAY_MS);
  };

  private readonly createTaskApi: ICreateTaskApi;
  private readonly getUsersApi: IGetUsersApi;
  private isCancelled: boolean;
}

export default CreateTaskTab;
