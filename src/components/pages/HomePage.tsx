import AlignedContainer from "../AlignedContainer";
import { Button } from "@mui/material";
import RegistrationDialog from "../dialogs/RegistrationDialog";
import LoginDialog from "../dialogs/LoginDialog";
import React, { useState } from "react";
import { IUserApi } from "../../core/IUserApi";
import { ISessionApi, UserDetails } from "../../core/ISessionApi";

interface HomePageProperties {
  readonly sessionApi: ISessionApi;
  readonly userApi: IUserApi;
  readonly onLogin: (user: UserDetails) => void;
}

function HomePage({
  sessionApi,
  userApi,
  onLogin,
}: HomePageProperties): JSX.Element {
  const [isRegistering, setIsRegistering] = useState(false);
  const [isLoggingIn, setIsLoggingIn] = useState(false);

  const setRegistering = () => setIsRegistering(true);
  const setNotRegistering = () => setIsRegistering(false);

  const setLoggingIn = () => setIsLoggingIn(true);
  const setNotLoggingIn = () => setIsLoggingIn(false);

  const handleLogin = () => {
    sessionApi.getLoggedInUser().then((loggedInUser) => onLogin(loggedInUser));
  };

  return (
    <>
      <h1>Welcome To Task Board!</h1>
      <AlignedContainer horizontalAlignment={"left"}>
        <Button onClick={setRegistering} disabled={isRegistering}>
          Sign up
        </Button>
        <Button onClick={setLoggingIn} disabled={isLoggingIn}>
          Sign in
        </Button>
      </AlignedContainer>
      <RegistrationDialog
        open={isRegistering}
        onClose={setNotRegistering}
        registrationApi={userApi}
      />
      <LoginDialog
        open={isLoggingIn}
        onClose={setNotLoggingIn}
        onLogin={handleLogin}
        loginApi={sessionApi}
      />
    </>
  );
}

export default HomePage;
