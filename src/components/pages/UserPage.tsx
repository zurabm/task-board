import { ISessionApi, UserDetails } from "../../core/ISessionApi";
import React, { useState } from "react";
import { ITaskApi } from "../../core/ITaskApi";
import { IconButton, Toolbar } from "@mui/material";
import TaskBoardDrawer from "../TaskBoardDrawer";
import { TaskBoardTab, TaskBoardTabManager } from "../tabs/TaskBoardTabManager";
import WelcomeTab from "../tabs/WelcomeTab";
import MenuOpenIcon from "@mui/icons-material/MenuOpen";
import TasksTab from "../tabs/TasksTab";
import LogoutDialog from "../dialogs/LogoutDialog";
import CreateTaskTab from "../tabs/CreateTaskTab";
import { LogoutRounded } from "@mui/icons-material";
import { IUserApi } from "../../core/IUserApi";

const DUE_SOON_DAYS = 1;

interface UserPageProperties {
  readonly user: UserDetails;
  readonly userApi: IUserApi;
  readonly taskApi: ITaskApi;
  readonly sessionApi: ISessionApi;
  readonly onLogout: () => void;
}

function UserPage({
  user,
  taskApi,
  userApi,
  sessionApi,
  onLogout,
}: UserPageProperties): JSX.Element {
  const [drawerVisible, setDrawerVisible] = useState(false);
  const [selectedTab, setSelectedTab] = useState(TaskBoardTab.WELCOME);
  const [isLoggingOut, setIsLoggingOut] = useState(false);

  const toggleDrawer = () => setDrawerVisible(!drawerVisible);

  const setLoggingOut = () => setIsLoggingOut(true);
  const setNotLoggingOut = () => setIsLoggingOut(false);

  const handleLogout = () => sessionApi.logoutUser().then(onLogout);

  const renderTab = (tab: TaskBoardTab) => {
    switch (tab) {
      case TaskBoardTab.WELCOME:
        return <WelcomeTab user={user} key={TaskBoardTab.WELCOME} />;
      case TaskBoardTab.ASSIGNED:
        return (
          <TasksTab
            title="Tasks assigned to you"
            taskSupplier={() =>
              taskApi.getTasks({ assignees: [user.fullName] })
            }
            taskDetailsApi={taskApi}
            key={TaskBoardTab.ASSIGNED}
          />
        );
      case TaskBoardTab.AUTHORED:
        return (
          <TasksTab
            title="Tasks created by you"
            taskSupplier={() => taskApi.getTasks({ author: user.fullName })}
            taskDetailsApi={taskApi}
            key={TaskBoardTab.AUTHORED}
          />
        );
      case TaskBoardTab.DUE_SOON:
        return (
          <TasksTab
            title="Tasks due soon"
            taskSupplier={() =>
              taskApi.getTasks({
                overdue: true,
                timeLeft: { days: DUE_SOON_DAYS },
              })
            }
            taskDetailsApi={taskApi}
            key={TaskBoardTab.DUE_SOON}
          />
        );
      case TaskBoardTab.CREATE:
        return (
          <CreateTaskTab
            createTaskApi={taskApi}
            getUsersApi={userApi}
            key={TaskBoardTab.CREATE}
          />
        );
    }
  };

  return (
    <>
      <Toolbar>
        <IconButton onClick={toggleDrawer}>
          <MenuOpenIcon />
          Tasks
        </IconButton>
      </Toolbar>
      <TaskBoardDrawer
        title={`${user.fullName}'s Task Board`}
        open={drawerVisible}
        onClose={toggleDrawer}
        renderActions={
          <IconButton size="small" onClick={setLoggingOut}>
            Log out <LogoutRounded />
          </IconButton>
        }
      >
        <TaskBoardTabManager
          onSelect={setSelectedTab}
          selectedTab={selectedTab}
        />
      </TaskBoardDrawer>
      <LogoutDialog
        open={isLoggingOut}
        logoutApi={sessionApi}
        onClose={setNotLoggingOut}
        onLogout={handleLogout}
      />
      {renderTab(selectedTab)}
    </>
  );
}

export default UserPage;
