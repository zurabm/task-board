import {
  Box,
  Button,
  Dialog,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@mui/material";
import AlignedContainer from "../AlignedContainer";
import React from "react";

interface ILogoutApi {
  /**
   * Logs out for the current user.
   */
  logoutUser(): Promise<boolean>;
}

interface LogoutDialogProperties {
  readonly open: boolean;
  readonly logoutApi: ILogoutApi;
  readonly onClose: () => void;
  readonly onLogout: () => void;
}

function LogoutDialog({
  open,
  onClose,
  onLogout,
}: LogoutDialogProperties): JSX.Element {
  return (
    <Dialog open={open} onClose={onClose} maxWidth={false}>
      <DialogTitle>
        <AlignedContainer>Confirm Log Out</AlignedContainer>
      </DialogTitle>
      <DialogContentText component="div">
        <Box margin={2}>Are you sure you want to log out?</Box>
      </DialogContentText>
      <DialogContent>
        <AlignedContainer>
          <Button onClick={onLogout}>Yes</Button>
          <Button onClick={onClose}>No</Button>
        </AlignedContainer>
      </DialogContent>
    </Dialog>
  );
}

export default LogoutDialog;
