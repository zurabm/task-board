import {
  Button,
  Dialog,
  DialogActions,
  DialogContentText,
  DialogTitle,
  Divider,
  Grid,
  Stack,
} from "@mui/material";
import React from "react";
import { Task } from "../../core/ITaskApi";
import AlignedContainer from "../AlignedContainer";
import DateDisplay from "../DateDisplay";
import StatusDisplay from "../StatusDisplay";

interface TaskDialogProperties {
  readonly task: Task | null;
  readonly onClose: () => void;
}

function TaskDialog({ task, onClose }: TaskDialogProperties): JSX.Element {
  const open = task != null;

  return (
    <Dialog open={open} onClose={onClose} maxWidth={false} fullWidth={true}>
      {open && (
        <Grid margin={2}>
          <DialogTitle>
            <small>Task {task?.id}</small> <br />
            <Stack direction="row" spacing={5}>
              <strong>{task?.title}</strong>
              <small>
                Status: <StatusDisplay status={task?.status} />
              </small>
            </Stack>
            <small>
              Due: <DateDisplay date={task?.due} />
            </small>
            <Divider />
          </DialogTitle>
          <DialogContentText component="div" margin={3}>
            <h2>Description</h2>
            <p>{task?.description}</p>
            <h3>Assignees</h3>
            <p>{task?.assignees.join(", ")}</p>
            <h3>Author</h3>
            <p>{task?.author}</p>
          </DialogContentText>
          <DialogActions>
            <AlignedContainer horizontalAlignment="right">
              <Divider />
              <Button onClick={onClose}>Close</Button>
            </AlignedContainer>
          </DialogActions>
        </Grid>
      )}
    </Dialog>
  );
}

export default TaskDialog;
