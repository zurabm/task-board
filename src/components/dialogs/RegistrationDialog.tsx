import {
  Alert,
  Box,
  Button,
  Dialog,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@mui/material";
import React, { useState } from "react";
import RegistrationForm from "../forms/RegistrationForm";
import { RegistrationDetails } from "../../core/IUserApi";
import AlignedContainer from "../AlignedContainer";

const REGISTRATION_DESCRIPTION = (
  <>
    Please register to view and create tasks on Task Board. <br />
    In the future we may send notifications to your email.
  </>
);

const SUCCESS_DESCRIPTION = (
  <Alert severity="success">Success! You're now a part of the team.</Alert>
);

const FAILURE_DESCRIPTION = (
  <Alert severity="error">Failure, oh oh! Something isn't right.</Alert>
);

const FORM_CLEAR_TIMEOUT_MS = 200;

interface IRegistrationApi {
  /**
   * Attempts to register a user with the specified {@link RegistrationDetails} and returns the result.
   */
  registerUser(registration: RegistrationDetails): Promise<boolean>;
}

interface RegistrationDialogProperties {
  readonly open: boolean;
  readonly onClose: () => void;
  readonly registrationApi: IRegistrationApi;
}

function RegistrationDialog({
  open,
  onClose,
  registrationApi,
}: RegistrationDialogProperties): JSX.Element {
  const [isRegistered, setIsRegistered] = useState<boolean | null>(null);

  const handleClose = () => {
    onClose();
    setTimeout(() => setIsRegistered(null), FORM_CLEAR_TIMEOUT_MS);
  };

  const handleSubmit = (registration: RegistrationDetails) => {
    registrationApi
      .registerUser(registration)
      .then((registrationResult) => setIsRegistered(registrationResult));
  };

  return (
    <Dialog open={open} onClose={handleClose} maxWidth={false}>
      <DialogTitle>Register</DialogTitle>
      <DialogContentText component="div">
        <Box margin={2}>
          {isRegistered === null && REGISTRATION_DESCRIPTION}
          {isRegistered === true && SUCCESS_DESCRIPTION}
          {isRegistered === false && FAILURE_DESCRIPTION}
        </Box>
      </DialogContentText>
      <DialogContent>
        {isRegistered == null && (
          <RegistrationForm onClose={handleClose} onSubmit={handleSubmit} />
        )}
        {isRegistered != null && (
          <AlignedContainer>
            <Button onClick={handleClose}>Close</Button>
          </AlignedContainer>
        )}
      </DialogContent>
    </Dialog>
  );
}

export default RegistrationDialog;
