import {
  Alert,
  Dialog,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@mui/material";
import React, { useState } from "react";
import LoginForm from "../forms/LoginForm";
import { LoginDetails } from "../../core/ISessionApi";

const FAILURE_DESCRIPTION = (
  <Alert severity="error">Failure! Could not log in.</Alert>
);

interface ILoginApi {
  /**
   * Authenticates a user with the specified {@link LoginDetails} and returns the result.
   */
  loginUser(login: LoginDetails): Promise<boolean>;
}

interface LoginDialogProperties {
  readonly open: boolean;
  readonly onClose: () => void;
  readonly onLogin: () => void;
  readonly loginApi: ILoginApi;
}

function LoginDialog({
  open,
  onClose,
  onLogin,
  loginApi,
}: LoginDialogProperties): JSX.Element {
  const [isLoggedIn, setIsLoggedIn] = useState<boolean | null>(null);

  const handleSubmit = (login: LoginDetails) => {
    loginApi.loginUser(login).then((loginResult) => {
      if (loginResult) {
        onLogin();
      }
      setIsLoggedIn(loginResult);
    });
  };

  return (
    <Dialog open={open} maxWidth={false}>
      <DialogTitle>Log in</DialogTitle>
      <DialogContentText component="div">
        {isLoggedIn === false && FAILURE_DESCRIPTION}
      </DialogContentText>
      <DialogContent>
        <LoginForm onClose={onClose} onSubmit={handleSubmit} />
      </DialogContent>
    </Dialog>
  );
}

export default LoginDialog;
