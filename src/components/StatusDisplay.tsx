import { TaskStatus } from "../core/ITaskApi";
import { Chip } from "@mui/material";
import { CheckCircleOutline, Circle, HdrStrong } from "@mui/icons-material";

const getChip = (status: TaskStatus): JSX.Element => {
  switch (status) {
    case TaskStatus.NOT_DONE:
      return <Chip label="Not Done" variant="outlined" icon={<Circle />} />;
    case TaskStatus.IN_PROGRESS:
      return (
        <Chip label="In Progress" variant="outlined" icon={<HdrStrong />} />
      );
    case TaskStatus.DONE:
      return (
        <Chip label="Done" variant="outlined" icon={<CheckCircleOutline />} />
      );
  }
};

const getLabel = (status: TaskStatus): JSX.Element => {
  switch (status) {
    case TaskStatus.NOT_DONE:
      return <>Not Done</>;
    case TaskStatus.IN_PROGRESS:
      return <>In Progress</>;
    case TaskStatus.DONE:
      return <>Done</>;
  }
};

interface StatusDisplayProperties {
  readonly status: TaskStatus;
  readonly asChip: boolean;
}

function StatusDisplay({
  status,
  asChip,
}: StatusDisplayProperties): JSX.Element {
  return asChip ? getChip(status) : getLabel(status);
}

StatusDisplay.defaultProps = {
  asChip: true,
};

export default StatusDisplay;
