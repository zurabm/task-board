import { Box, Drawer, Grid, IconButton, Paper } from "@mui/material";
import React from "react";
import { ArrowBackRounded } from "@mui/icons-material";
import StyledText from "./StyledText";

interface TaskBoardDrawerProperties {
  readonly title: string;
  readonly open: boolean;
  readonly onClose: () => void;
  readonly renderActions?: JSX.Element;
  readonly children?: any;
}

function TaskBoardDrawer({
  title,
  open,
  onClose,
  renderActions,
  children,
}: TaskBoardDrawerProperties): JSX.Element {
  return (
    <Drawer anchor="left" open={open} onClose={onClose}>
      <Box marginTop={1} marginBottom={1}>
        <IconButton onClick={onClose} size="medium">
          <ArrowBackRounded /> Close
        </IconButton>
      </Box>
      <Paper variant="outlined" square>
        <Box margin={4}>
          <StyledText type="h4">{title}</StyledText>
        </Box>
      </Paper>
      {children}
      <Paper variant="outlined" square>
        <Grid
          item
          container
          justifyContent="center"
          marginTop={4}
          marginBottom={4}
        >
          {renderActions}
        </Grid>
      </Paper>
    </Drawer>
  );
}

export default TaskBoardDrawer;
